 
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.rmi.Naming;
import java.rmi.NotBoundException;

 
 
// An AWT GUI program inherits the top-level container java.awt.Frame
public class Gui extends Frame implements ActionListener,WindowListener  {
   private TextField tfCount;
  // private Button btnCountUp, btnCountDown, btnReset;
   private int count = 0;
   private Button btnTax;
   private Button btnSell;
   private Label lblInput;     // Declare input Label
   private Label lblOutput;    // Declare output Label
   private Label lblInput2;     // Declare input Label
   private Label lblOutput2;    // Declare output Label
   private TextField tfInput;  // Declare input TextField
   private TextField tfOutput; // Declare output TextField
   private TextField tfInput2;  // Declare input TextField
   private TextField tfInput3;  // Declare input TextField
   private TextField tfOutput2; // Declare output TextField
   private Label lblInput3;
 
   // Constructor to setup the GUI components and event handlers
   public Gui () {
      setLayout(new FlowLayout());
      /*add(new Label("Car"));   // an anonymous instance of Label
      tfCount = new TextField("0", 10);
      tfCount.setEditable(false);  // read-only
      add(tfCount);                // "super" Frame adds tfCount
 */
      
 
      
      lblInput = new Label("Year of car: "); // Construct Label
      add(lblInput);               // "super" Frame container adds Label component
 
      tfInput = new TextField(10); // Construct TextField
      add(tfInput);                // "super" Frame adds TextField
 
      tfInput.addActionListener(this);
         // "tfInput" is the source object that fires an ActionEvent upon entered.
         // The source add "this" instance as an ActionEvent listener, which provides
         //  an ActionEvent handler called actionPerformed().
         // Hitting "enter" on tfInput invokes actionPerformed().
 
     
      
      
      lblInput2 = new Label("engine size: "); // Construct Label
      add(lblInput2);               // "super" Frame container adds Label component
 
      tfInput2 = new TextField(10); // Construct TextField
      add(tfInput2);                // "super" Frame adds TextField
 
      tfInput2.addActionListener(this);
         // "tfInput" is the source object that fires an ActionEvent upon entered.
         // The source add "this" instance as an ActionEvent listener, which provides
         //  an ActionEvent handler called actionPerformed().
         // Hitting "enter" on tfInput invokes actionPerformed().
 
      
      lblInput3 = new Label("price purchasing: "); // Construct Label
      add(lblInput3);               // "super" Frame container adds Label component
 
      tfInput3 = new TextField(10); // Construct TextField
      add(tfInput3);                // "super" Frame adds TextField
 
      tfInput3.addActionListener(this);
      
      btnTax = new Button("Tax for a car");
      add(btnTax);
      // Construct an anonymous instance of an anonymous inner class.
      // The source Button adds the anonymous instance as ActionEvent listener
      btnTax.addActionListener(new ActionListener() {
         @Override
        // @SuperWarrning
         public void actionPerformed(ActionEvent evt) {
        	 ITaxService taxService = null;
        	 try {
				taxService = (ITaxService) Naming.lookup("rmi://localhost:5509/tax");
				int y=Integer.parseInt(tfInput.getText());
				int e=Integer.parseInt(tfInput2.getText());
				double x=taxService.computeTax(new Car(y,e));
				tfOutput.setText(x + "");
			} catch (IOException | NotBoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	 
         }
      });
      
      lblOutput = new Label("The Tax is: ");  // allocate Label
      add(lblOutput);               // "super" Frame adds Label
 
      tfOutput = new TextField(10); // allocate TextField
      tfOutput.setEditable(false);  // read-only
      add(tfOutput);                // "super" Frame adds TextField
      
      btnSell = new Button("Selling price");
      add(btnSell);
      btnSell.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent evt) {
        	 
        	 
        	 ITaxService pretVanzare = null;
        	 
        	 try {
        		 pretVanzare = (ITaxService) Naming.lookup("rmi://localhost:5509/tax");
        		 int y=Integer.parseInt(tfInput.getText());
 				int e=Integer.parseInt(tfInput2.getText());
 				double p=Double.parseDouble(tfInput3.getText());
 				double xx=pretVanzare.priceSelling(new Car(y, e,p));
 				tfOutput2.setText(xx + "");
 			} catch (IOException e) {
 				// TODO Auto-generated catch block
 				e.printStackTrace();
 			} catch (NotBoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        	 
        	 
        	 
        	 
        	 
           // count--;
           // tfCount.setText(count + "");
         }
      });
      lblOutput2 = new Label("The Selling Price is: ");  // allocate Label
      add(lblOutput2);               // "super" Frame adds Label
 
      tfOutput2 = new TextField(10); // allocate TextField
      tfOutput2.setEditable(false);  // read-only
      add(tfOutput2);                // "super" Frame adds TextField
      
      
      
      addWindowListener(this);
      setTitle("Car properties");
      setSize(750, 400);
      setVisible(true);
   }
 
   // The entry main method
   public static void main(String[] args) {
      new Gui();  // Let the constructor do the job
   }

@Override
public void windowActivated(WindowEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void windowClosed(WindowEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void windowClosing(WindowEvent e) {
	// TODO Auto-generated method stub
	 System.exit(0);
}

@Override
public void windowDeactivated(WindowEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void windowDeiconified(WindowEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void windowIconified(WindowEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void windowOpened(WindowEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void actionPerformed(ActionEvent e) {
	// TODO Auto-generated method stub
	
}
}