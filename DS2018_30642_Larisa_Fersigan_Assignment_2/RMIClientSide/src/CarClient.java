import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class CarClient {

	public static void main (String[] args)throws MalformedURLException, RemoteException, NotBoundException {
		ITaxService service=(ITaxService)Naming.lookup("rmi://localhost:5509/tax");
		System.out.println("Tax value: "	+ service.computeTax(new Car(2009, 2000)));
}
}
