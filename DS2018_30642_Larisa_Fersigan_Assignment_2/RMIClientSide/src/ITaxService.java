
public interface ITaxService {
	double computeTax(Car c);
	 
	double priceSelling(Car c);
}
