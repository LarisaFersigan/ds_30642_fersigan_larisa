import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Compute extends UnicastRemoteObject  implements ITaxService{

	protected Compute() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	public double computeTax(Car c) {
		// Dummy formula
		if (c.getEngineCapacity() <= 0) {
			throw new IllegalArgumentException("Engine capacity must be positive.");
		}
		int sum = 8;
		if(c.getEngineCapacity() > 1601) sum = 18;
		if(c.getEngineCapacity() > 2001) sum = 72;
		if(c.getEngineCapacity() > 2601) sum = 144;
		if(c.getEngineCapacity() > 3001) sum = 290;
		return c.getEngineCapacity() / 200.0 * sum;
	}

	@Override
	public double priceSelling(Car c) {
		// TODO Auto-generated method stub
		//return 0;
		double priceSell;
		double price=c.getPurchasingPrice();
		if((2018-c.getYear())<7)
			priceSell=price-((price*(2018-c.getYear())/7));
			//priceSell=price-(price/7)*(2018-c.getYear());
		
		else 
			priceSell=0;
		return priceSell;
		
	}
}
