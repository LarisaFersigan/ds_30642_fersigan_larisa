import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ITaxService extends Remote{
	double computeTax(Car c) throws RemoteException;
	 
	double priceSelling(Car c) throws RemoteException;
}
